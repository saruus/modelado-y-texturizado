﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModificadorColorAleatorio : MonoBehaviour {

	public Color newColor = new Color (Random.value, Random.value, Random.value, 1.0f);
	private Color colororiginal;







	// Use this for initialization
	void Start () {
		colororiginal = gameObject.GetComponent<Renderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("c"))
		{
			gameObject.GetComponent<Renderer>().material.color = newColor;
		}
		else {
			gameObject.GetComponent<Renderer>().material.color = colororiginal;
		}
	}
}
