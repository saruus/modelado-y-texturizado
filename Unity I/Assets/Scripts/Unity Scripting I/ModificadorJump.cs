﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModificadorJump : MonoBehaviour {

	public Color Color = Color.blue;
	private Color colororiginal;







	// Use this for initialization
	void Start () {
		colororiginal = gameObject.GetComponent<Renderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("space"))
		{
			gameObject.GetComponent<Renderer>().material.color = Color;
		}
		else {
			gameObject.GetComponent<Renderer>().material.color = colororiginal;
		}
	}
}
