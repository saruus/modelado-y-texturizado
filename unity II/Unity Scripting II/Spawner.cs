﻿using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objectToSpawn;
	public GameObject centro; 
	public float minScale =1;
	public float maxScale=10;
	public float increment;
	public float minForce = 1;
	public float maxForce = 10;
	public float radious;
	public float limit;
	Renderer render;
	public Color Color;

	private void Start()
	{
		render = GetComponent<Renderer>();
		increment = 0f;
	}

	void Update () {
		increment += Time.deltaTime;

        if (increment>=limit) {
			increment = 0f; 

            GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(minScale, maxScale);
			transform.RotateAround(centro.transform.position, Vector3.up, 20 * Time.deltaTime);
			transform.Translate(Vector3.forward * radious * Time.deltaTime);
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
			Vector3 direction = new Vector3();

			int mappedDirection = Random.Range(0, 3);
			switch (mappedDirection) {
				case 0:
					direction = Vector3.right;
					break;
				case 1:
					direction = Vector3.left;
					break;
				case 2:
					direction = Vector3.up;
					break;
				case 3:
					direction = Vector3.down;
					break;
			
			}
			render.material.SetColor("_Color", Color.green);
            rb.AddForce(direction * Random.Range(minForce, maxForce), ForceMode.Impulse);
        }
	}
}

